package games;

public class FizzBuzz {
    private FizzBuzz() {
    }

    public static String convert(Integer input) throws OutOfRangeException {
        if (outOfRange(input)) {
            throw new OutOfRangeException();
        }
        return doConvert(input);
    }

    private static boolean outOfRange(Integer input) {
        return input <= 0 || input > 100;
    }

    private static String doConvert(Integer input) {
        if (isFizz(input) && isBuzz(input)) {
            return "FizzBuzz";
        }
        if (isFizz(input)) {
            return "Fizz";
        }
        if (isBuzz(input)) {
            return "Buzz";
        }
        return input.toString();
    }

    private static boolean isFizz(Integer input) {
        return input % 3 == 0;
    }

    private static boolean isBuzz(Integer input) {
        return input % 5 == 0;
    }
}
