package greeting;

import java.util.List;

public class Greeter {
    String formality;

    public String greet(List<GreetingStrategy> strategies) {
        return strategies.stream()
                         .filter(strategy -> strategy.isApplyable(this.formality))
                         .findFirst()
                         .orElseThrow()
                         .greet();
    }

    public void setFormality(String formality) {
        this.formality = formality;
    }
}
