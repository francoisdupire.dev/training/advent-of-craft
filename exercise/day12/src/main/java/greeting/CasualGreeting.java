package greeting;

public class CasualGreeting implements GreetingStrategy {
    @Override
    public boolean isApplyable(String formality) {
        return formality != null && formality.equals("casual");
    }
    @Override
    public String greet() {
        return "Sup bro?";
    }
}
