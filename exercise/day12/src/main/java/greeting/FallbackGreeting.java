package greeting;

public class FallbackGreeting implements GreetingStrategy {
    @Override
    public boolean isApplyable(String ignored) {
        return true;
    }
    @Override
    public String greet() {
        return "Hello.";
    }
}
