package greeting;

public class IntimateGreeting implements GreetingStrategy {
    @Override
    public boolean isApplyable(String formality) {
        return formality != null && formality.equals("intimate");
    }

    @Override
    public String greet() {
        return "Hello Darling!";
    }
}
