package greeting;

public interface GreetingStrategy {
    boolean isApplyable(String formality);
    String greet();
}
