package greeting;

public class FormalGreeting implements GreetingStrategy {
    @Override
    public boolean isApplyable(String formality) {
        return formality != null && formality.equals("formal");
    }
    @Override
    public String greet() {
        return "Good evening, sir.";
    }
}
