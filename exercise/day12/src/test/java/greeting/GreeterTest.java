package greeting;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class GreeterTest {
    @Test
    void saysHello() {
        var greeter = new Greeter();

        assertThat(greeter.greet(strategies()))
                .isEqualTo("Hello.");
    }

    @Test
    void saysHelloFormally() {
        var greeter = new Greeter();
        greeter.setFormality("formal");

        assertThat(greeter.greet(strategies()))
                .isEqualTo("Good evening, sir.");
    }

    @Test
    void saysHelloCasually() {
        var greeter = new Greeter();
        greeter.setFormality("casual");

        assertThat(greeter.greet(strategies()))
                .isEqualTo("Sup bro?");
    }

    @Test
    void saysHelloIntimately() {
        var greeter = new Greeter();
        greeter.setFormality("intimate");

        assertThat(greeter.greet(strategies()))
                .isEqualTo("Hello Darling!");
    }

    private static List<GreetingStrategy> strategies() {
        return List.of(new FormalGreeting(),
                       new CasualGreeting(),
                       new IntimateGreeting(),
                       new FallbackGreeting());
    }
}
