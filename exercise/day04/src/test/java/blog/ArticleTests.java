package blog;

import org.assertj.core.api.ObjectAssert;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Random;

import static blog.ArticleTests.ArticleAssert.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ArticleTests {
    private static final String DEFAULT_ARTICLE = "Amazing article !!!";
    private static final String DEFAULT_AUTHOR = "Pablo Escobar";

    @Test
    void aNewArticleShouldHaveNoComment() {
        var article = aNewArticle();

        assertThat(article).hasNoComments();
    }

    @Test
    void articleShouldHaveOneCommentWhenOneHasBeenAdded() throws CommentAlreadyExistException {
        var article = aNewArticle();

        article.addComment(DEFAULT_ARTICLE, DEFAULT_AUTHOR);

        assertThat(article).hasXComments(1);
    }

    @RepeatedTest(10)
    void articleShouldHaveAsMuchCommentsAsHasBeenAdded() throws CommentAlreadyExistException {
        var article = aNewArticle();

        int commentsNumber = new Random().nextInt(100);
        for (int i = 0; i < commentsNumber; i++) {
            article.addComment("%s (%d)".formatted(DEFAULT_ARTICLE, i), DEFAULT_AUTHOR);
        }

        assertThat(article).hasXComments(commentsNumber);
    }

    @Test
    void articleCommentsShouldMatchAddedOnes() throws CommentAlreadyExistException {
        var article = aNewArticle();

        article.addComment(DEFAULT_ARTICLE, DEFAULT_AUTHOR);

        assertThat(article).hasComment(new Comment(DEFAULT_ARTICLE, DEFAULT_AUTHOR, LocalDate.now()));
    }

    @Test
    void shouldNotBePossibleToAddSameCommentTwice() throws CommentAlreadyExistException {
        var article = aNewArticle();
        article.addComment(DEFAULT_ARTICLE, DEFAULT_AUTHOR);

        assertThatThrownBy(() -> article.addComment(DEFAULT_ARTICLE, DEFAULT_AUTHOR))
                .isInstanceOf(CommentAlreadyExistException.class);
    }

    private static Article aNewArticle() {
        return new Article("Lorem Ipsum",
                           "consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore");
    }

    public static class ArticleAssert extends ObjectAssert<Article> {
        private ArticleAssert(Article article) {
            super(article);
        }

        public static ArticleAssert assertThat(Article article) {
            return new ArticleAssert(article);
        }

        public ArticleAssert hasNoComments() {
            if (!actual.getComments().isEmpty()) {
                failWithMessage("Article should have no comments, but has %d",
                                actual.getComments().size());
            }
            return this;
        }

        public ArticleAssert hasXComments(int commentsNumber) {
            if (actual.getComments().size() != commentsNumber) {
                failWithMessage("Article should have %d comments, but has %d",
                                commentsNumber,
                                actual.getComments().size());
            }
            return this;
        }

        public ArticleAssert hasComment(Comment comment) {
            if (!actual.getComments().contains(comment)) {
                failWithMessage("Article should have comment %s, but has not", comment);
            }
            return this;
        }
    }
}
