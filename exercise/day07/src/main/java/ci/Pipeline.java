package ci;

import ci.dependencies.*;

public class Pipeline {
    private final Config config;
    private final Emailer emailer;
    private final Logger log;

    public Pipeline(Config config, Emailer emailer, Logger log) {
        this.config = config;
        this.emailer = emailer;
        this.log = log;
    }

    public void run(Project project) {
        log(project);
        sendNotification(project);
    }

    private void log(Project project) {
        logTestsStatus(project);
        logDeploymentStatus(project);
        logEmailingStatus();
    }

    private void logTestsStatus(Project project) {
        switch (project.testStatus()) {
            case NO_TESTS:
                log.info("No tests");
                break;
            case PASSING_TESTS:
                log.info("Tests passed");
                break;
            case FAILING_TESTS:
                log.error("Tests failed");
                break;
        }
    }

    private void logDeploymentStatus(Project project) {
        if (testsPassed(project)) {
            if (project.deployed()) {
                log.info("Deployment successful");
            } else {
                log.error("Deployment failed");
            }
        }
    }

    private void sendNotification(Project project) {
        if (config.sendEmailSummary()) {
            emailer.send(notification(project));
        }
    }

    private void logEmailingStatus() {
        if (config.sendEmailSummary()) {
            log.info("Sending email");
        } else {
            log.info("Email disabled");
        }
    }

    private String notification(Project project) {
        String result;
        if (testsPassed(project)) {
            if (deploymentSuccessful(project)) {
                result = "Deployment completed successfully";
            } else {
                result = "Deployment failed";
            }
        } else {
            result = "Tests failed";
        }
        return result;
    }

    private boolean testsPassed(Project project) {
        return project.testStatus() != TestStatus.FAILING_TESTS;
    }

    private boolean deploymentSuccessful(Project project) {
        return testsPassed(project) && project.deployed();
    }
}
