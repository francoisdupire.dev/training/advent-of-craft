package ci;

import ci.dependencies.Config;
import ci.dependencies.Emailer;
import ci.dependencies.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static ci.dependencies.TestStatus.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.*;
import static org.mockito.Mockito.*;

class PipelineTest {
    private final Config config = mock(Config.class);
    private final CapturingLogger log = new CapturingLogger();
    private final Emailer emailer = mock(Emailer.class);

    private Pipeline pipeline;

    @BeforeEach
    void setUp() {
        pipeline = new Pipeline(config, emailer, log);
    }

    @ParameterizedTest
    @MethodSource("projects")
    void shouldNotSendEmailNotificationIfDisabled(Project project, String ignored) {
        givenEmailNotificationDisabled();

        pipeline.run(project);

        thenNoEmailNotificationSent();
    }

    @ParameterizedTest
    @MethodSource("projects")
    void shouldSendEmailNotificationIfEnabled(Project project, String notification) {
        givenEmailNotificationEnabled();

        pipeline.run(project);

        thenEmailNotificationSent(notification);
    }

    @ParameterizedTest
    @MethodSource("projects")
    void shouldLogEmailDisabledIfDisabled(Project project, String ignored) {
        givenEmailNotificationDisabled();

        pipeline.run(project);

        thenLogged("INFO: Email disabled");
    }
    
    @ParameterizedTest
    @MethodSource("projects")
    void shouldLogSendingEmailIfEnabled(Project project, String ignored) {
        givenEmailNotificationEnabled();

        pipeline.run(project);

        thenLogged("INFO: Sending email");
    }

    @ParameterizedTest
    @MethodSource("testsStatusLogs")
    void shouldLogTestsStatus(Project project, String testsStatusLog) {
        pipeline.run(project);

        thenLogged(testsStatusLog);
    }

    @ParameterizedTest
    @MethodSource("deploymentStatusLogs")
    void shouldLogDeploymentStatus(Project project, String deploymentStatusLog) {
        pipeline.run(project);

        thenLogged(deploymentStatusLog);
    }

    @Test
    void shouldLogInOrder() {
        Project project = greenProjectBuilder().build();

        pipeline.run(project);

        thenLoggedInOrder("INFO: Tests passed",
                          "INFO: Deployment successful",
                          "INFO: Email disabled");
    }

    public static Stream<Arguments> projects() {
        return Stream.of(
                arguments(greenProjectBuilder().build(),
                          "Deployment completed successfully"),
                arguments(greenProjectBuilder().setTestStatus(FAILING_TESTS)
                                               .build(),
                          "Tests failed"),
                arguments(greenProjectBuilder().setTestStatus(NO_TESTS)
                                               .build(),
                          "Deployment completed successfully"),
                arguments(greenProjectBuilder().setDeploysSuccessfully(false)
                                               .build(),
                          "Deployment failed"),
                arguments(greenProjectBuilder().setDeploysSuccessfully(false)
                                               .setTestStatus(FAILING_TESTS)
                                               .build(),
                          "Tests failed"),
                arguments(greenProjectBuilder().setDeploysSuccessfully(false)
                                               .setTestStatus(NO_TESTS)
                                               .build(),
                          "Deployment failed")
        );
    }

    public static Stream<Arguments> testsStatusLogs() {
        return Stream.of(
                arguments(greenProjectBuilder().build(),
                          "INFO: Tests passed"),
                arguments(greenProjectBuilder().setTestStatus(FAILING_TESTS)
                                               .build(),
                          "ERROR: Tests failed"),
                arguments(greenProjectBuilder().setTestStatus(NO_TESTS)
                                               .build(),
                          "INFO: No tests")
        );
    }

    public static Stream<Arguments> deploymentStatusLogs() {
        return Stream.of(
                arguments(greenProjectBuilder().build(),
                          "INFO: Deployment successful"),
                arguments(greenProjectBuilder().setDeploysSuccessfully(false)
                                               .build(),
                          "ERROR: Deployment failed")
        );
    }

    private static Project.ProjectBuilder greenProjectBuilder() {
        return Project.builder()
                      .setTestStatus(PASSING_TESTS)
                      .setDeploysSuccessfully(true);
    }

    private void givenEmailNotificationEnabled() {
        givenEmailNotification(true);
    }

    private void givenEmailNotificationDisabled() {
        givenEmailNotification(false);
    }

    private void givenEmailNotification(boolean enabled) {
        when(config.sendEmailSummary()).thenReturn(enabled);
    }

    private void thenLogged(String line) {
        assertThat(log.getLoggedLines()).contains(line);
    }

    private void thenLoggedInOrder(String... lines) {
        assertThat(log.getLoggedLines()).containsExactly(lines);
    }

    private void thenEmailNotificationSent(String message) {
        verify(emailer).send(message);
    }

    private void thenNoEmailNotificationSent() {
        verify(emailer, never()).send(any());
    }
}