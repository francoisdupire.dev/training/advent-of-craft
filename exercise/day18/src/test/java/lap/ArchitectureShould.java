package lap;

import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;

class ArchitectureShould {

    @Test
    void notPermitConsequentUnderscoresInFieldNames() {
        ArchRule noConsequentUnderscores = noFields().should()
                                                     .haveNameMatching(".*__.*");

        noConsequentUnderscores.check(new ClassFileImporter().importPackages("lap"));
    }

    @Test
    void notPermitConsequentUnderscoresInClassNames() {
        ArchRule noConsequentUnderscores = noClasses().should()
                                                      .haveNameMatching(".*__.*");

        noConsequentUnderscores.check(new ClassFileImporter().importPackages("lap"));
    }

    @Test
    void notPermitConsequentUnderscoresInCodeUnitsNames() {
        ArchRule noConsequentUnderscores = noCodeUnits().should()
                                                        .haveNameMatching(".*__.*");

        noConsequentUnderscores.check(new ClassFileImporter().importPackages("lap"));
    }

    @Test
    void notPermitNamesStartingWithUnderscore() {
        ArchRule noConsequentUnderscores = noFields().should()
                                                     .haveNameMatching("_.*");

        noConsequentUnderscores.check(new ClassFileImporter().importPackages("lap"));
    }

    @Test
    void notPermitGettersToReturnVoid() {
        ArchRule noConsequentUnderscores = noMethods().that().haveNameStartingWith("get")
                                                      .should()
                                                      .haveRawReturnType("void");

        noConsequentUnderscores.check(new ClassFileImporter().importPackages("lap"));
    }

    @Test
    void notPermitIsMethodToReturnSomethingElseThanBooleanPrimitive() {
        ArchRule noConsequentUnderscores = methods().that().haveNameStartingWith("is")
                                                    .should()
                                                    .haveRawReturnType(boolean.class);

        noConsequentUnderscores.check(new ClassFileImporter().importPackages("lap"));
    }
}