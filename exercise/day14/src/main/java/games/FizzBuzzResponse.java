package games;

import java.util.Optional;

public class FizzBuzzResponse {
    private final String value;
    private final boolean outOfRange;

    private FizzBuzzResponse(String value, boolean outOfRange) {
        this.value = value;
        this.outOfRange = outOfRange;
    }

    public static FizzBuzzResponse of(String value) {
        return new FizzBuzzResponse(value, false);
    }

    public static FizzBuzzResponse outOfRange() {
        return new FizzBuzzResponse(null, true);
    }
}
