package password;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.*;

class PasswordShould {
    @ParameterizedTest
    @MethodSource("safePasswords")
    void beSafe(String safePassword) {
        assertThat(Password.from(safePassword)).isNotNull();
    }

    @Test
    void containAtLeastEightCharacters() {
        assertThatThrownBy(() -> Password.from("1234567"))
                .isInstanceOf(Password.Unsafe.class);
    }

    @ParameterizedTest
    @MethodSource("passwordsWithoutUppercase")
    void containAtLeastOneUppercase(String passwordWithoutUppercase) {
        assertThatThrownBy(() -> Password.from(passwordWithoutUppercase))
                .isInstanceOf(Password.Unsafe.class);
    }

    @ParameterizedTest
    @MethodSource("passwordsWithoutLowercase")
    void containAtLeastOneLowercase(String passwordWithoutLowercase) {
        assertThatThrownBy(() -> Password.from(passwordWithoutLowercase))
                .isInstanceOf(Password.Unsafe.class);
    }

    @ParameterizedTest
    @MethodSource("passwordsWithoutDigit")
    void containAtLeastOneDigit(String passwordWithoutDigit) {
        assertThatThrownBy(() -> Password.from(passwordWithoutDigit))
                .isInstanceOf(Password.Unsafe.class);
    }

    @ParameterizedTest
    @MethodSource("passwordWithoutSpecialCharacter")
    void containAtLeastOneSpecialCharacter(String passwordWithoutSpecialCharacter) {
        assertThatThrownBy(() -> Password.from(passwordWithoutSpecialCharacter))
                .isInstanceOf(Password.Unsafe.class);
    }

    @ParameterizedTest
    @MethodSource("passwordWithUnauthorizedCharacter")
    void notContainUnauthorizedCharacter(String passwordWithUnauthorizedCharacter) {
        assertThatThrownBy(() -> Password.from(passwordWithUnauthorizedCharacter))
                .isInstanceOf(Password.Unsafe.class);
    }

    public static Stream<Arguments> safePasswords() {
        return Stream.of(
                arguments("U@reSaf3"),
                arguments("Unbr3ak@bl"),
                arguments("Als0S#fe"),
                arguments("Als0$afe"),
                arguments("Als*Saf3"),
                arguments("Als%Saf3"),
                arguments("Als0Saf&")
        );
    }

    public static Stream<Arguments> passwordsWithoutUppercase() {
        return Stream.of(
                arguments("v3rys@fe"),
                arguments("s3cure@shell")
        );
    }

    public static Stream<Arguments> passwordsWithoutLowercase() {
        return Stream.of(
                arguments("V3RYS@FE"),
                arguments("S3CURE@SHELL")
        );
    }

    public static Stream<Arguments> passwordsWithoutDigit() {
        return Stream.of(
                arguments("VeryS@fe"),
                arguments("Secure@sHell")
        );
    }

    public static Stream<Arguments> passwordWithoutSpecialCharacter() {
        return Stream.of(
                arguments("V3rySafe"),
                arguments("S3cureAsHell")
        );
    }

    public static Stream<Arguments> passwordWithUnauthorizedCharacter() {
        return Stream.of(
                arguments("V3ryS@fe!"),
                arguments("S3cure@sH€ll")
        );
    }
}
