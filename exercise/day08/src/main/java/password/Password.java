package password;

import java.util.stream.Stream;

public class Password {
    public static Password from(String sequence) {
        if (unsafe(sequence)) {
            throw new Unsafe();
        }

        return new Password();
    }

    private static boolean unsafe(String sequence) {
        return rules().anyMatch(rule -> rule.apply(sequence));
    }

    private static Stream<UnsafeRule> rules() {
        return Stream.of(
                Password::tooShort,
                Password::noUppercase,
                Password::noLowercase,
                Password::noDigit,
                Password::noSpecialCharacter,
                Password::unauthorizedCharacter
        );
    }

    private static boolean tooShort(String sequence) {
        return sequence.length() < 8;
    }

    private static boolean noUppercase(String sequence) {
        return !sequence.matches(".*[A-Z].*");
    }

    private static boolean noLowercase(String sequence) {
        return !sequence.matches(".*[a-z].*");
    }

    private static boolean noDigit(String sequence) {
        return !sequence.matches(".*[0-9].*");
    }

    private static boolean noSpecialCharacter(String sequence) {
        return !sequence.matches(".*[@#$*%&.].*");
    }

    private static boolean unauthorizedCharacter(String sequence) {
        return !sequence.matches("[A-Za-z0-9@#$*%&.]+");
    }

    @FunctionalInterface
    private interface UnsafeRule {
        boolean apply(String sequence);
    }

    public static class Unsafe extends RuntimeException {}
}
