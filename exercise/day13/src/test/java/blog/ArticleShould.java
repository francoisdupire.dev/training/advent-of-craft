package blog;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ArticleShould {
    public static final String AUTHOR = "Pablo Escobar";
    private static final String COMMENT_TEXT = "Amazing article !!!";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 5})
    void haveAsMuchCommentsAsAdded(int numberOfComments) throws CommentAlreadyExistException {
        Article article = newArticle();
        comment(article, numberOfComments);

        assertThat(article.getComments()).hasSize(numberOfComments);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 5})
    void haveCommentsAddedInOrder(int numberOfComments) throws CommentAlreadyExistException {
        Article article = newArticle();
        comment(article, numberOfComments);

        assertThat(article.getComments().getLast())
                .isEqualTo(new Comment(COMMENT_TEXT + numberOfComments, AUTHOR, LocalDate.now()));
    }

    @Nested
    class Fail {

        @Test
        void whenSpamming() throws CommentAlreadyExistException {
            Article article = commentedArticle();

            assertThatThrownBy(() -> article.addComment(COMMENT_TEXT, AUTHOR))
                    .isInstanceOf(CommentAlreadyExistException.class);
        }
    }
    public static Article commentedArticle() throws CommentAlreadyExistException {
        Article result = newArticle();
        result.addComment(COMMENT_TEXT, AUTHOR);
        return result;
    }

    private static Article newArticle() {
        return new Article(
                "Lorem Ipsum",
                "consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore"
        );
    }

    private static void comment(Article article, int times) throws CommentAlreadyExistException {
        for (int i = 1; i <= times; i++) {
            article.addComment(COMMENT_TEXT + i, AUTHOR);
        }
    }
}
