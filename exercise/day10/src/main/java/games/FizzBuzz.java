package games;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class FizzBuzz {
    public static final int MIN = 0;
    public static final int MAX = 100;
    public static final int FIZZ = 3;
    public static final int BUZZ = 5;
    public static final int FIZZBUZZ = 15;

    private FizzBuzz() {
    }

    public static String convert(Integer input) {
        return appliableStrategy(input).applyTo(input);
    }

    private static Strategy appliableStrategy(Integer input) {
        return strategies()
                .filter(strategy -> strategy.isApplyableTo(input))
                .findFirst()
                .orElseThrow();
    }

    private static Stream<Strategy> strategies() {
        return Stream.of(
                new ComplexStrategy(FizzBuzz::isOutOfRange, ignored -> { throw new OutOfRangeException(); }),
                new ConstantOutputStrategy(FizzBuzz::isFizzBuzz, "FizzBuzz"),
                new ConstantOutputStrategy(FizzBuzz::isFizz, "Fizz"),
                new ConstantOutputStrategy(FizzBuzz::isBuzz, "Buzz"),
                new ComplexStrategy(ignored -> true, String::valueOf)
        );
    }

    private static boolean isFizzBuzz(Integer input) {
        return is(FIZZBUZZ, input);
    }

    private static boolean isFizz(Integer input) {
        return is(FIZZ, input);
    }

    private static boolean isBuzz(Integer input) {
        return is(BUZZ, input);
    }

    private static boolean is(Integer divisor, Integer input) {
        return input % divisor == 0;
    }

    private static boolean isOutOfRange(Integer input) {
        return input <= MIN || input > MAX;
    }

    private interface Strategy {
        boolean isApplyableTo(Integer input);
        String applyTo(Integer input);
    }

    private record ComplexStrategy(Predicate<Integer> isApplyable,
                                   Function<Integer, String> applicator) implements Strategy {
        public boolean isApplyableTo(Integer input) {
            return isApplyable.test(input);
        }

        public String applyTo(Integer input) {
            return applicator.apply(input);
        }
    }

    private record ConstantOutputStrategy(Predicate<Integer> isApplyable, String output) implements Strategy {
        public boolean isApplyableTo(Integer input) {
            return isApplyable.test(input);
        }

        public String applyTo(Integer input) {
            return output;
        }
    }
}
