package games;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.*;

class FizzBuzzTests {
    @ParameterizedTest
    @MethodSource("neitherFizzNorBuzz")
    void shouldReturnNumberWhenNeitherFizzNorBuzzNumber(int neitherFizzNorBuzzNumber, String numberAsString) throws OutOfRangeException {
        assertThat(FizzBuzz.convert(neitherFizzNorBuzzNumber))
                .isEqualTo(numberAsString);
    }

    @ParameterizedTest
    @ValueSource(ints = {3, 66, 99})
    void shouldReturnFizzWhenFizzNumber(int fizzNumber) throws OutOfRangeException {
        assertThat(FizzBuzz.convert(fizzNumber))
                .isEqualTo("Fizz");
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 50, 85})
    void shouldReturnBuzzWhenBuzzNumber(int buzzNumber) throws OutOfRangeException {
        assertThat(FizzBuzz.convert(buzzNumber))
                .isEqualTo("Buzz");
    }

    @ParameterizedTest
    @ValueSource(ints = {15, 30, 45})
    void shouldReturnFizzBuzzWhenFizzAndBuzzNumber(int fizzAndBuzzNumber) throws OutOfRangeException {
        assertThat(FizzBuzz.convert(fizzAndBuzzNumber))
                .isEqualTo("FizzBuzz");
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 101, -1})
    void shouldNotAllowNumbersOutsideOfRange(int outsideOfRangeNumber) {
        assertThatThrownBy(() -> FizzBuzz.convert(outsideOfRangeNumber))
                .isInstanceOf(OutOfRangeException.class);
    }

    public static Stream<Arguments> neitherFizzNorBuzz() {
        return Stream.of(
                arguments(1, "1"),
                arguments(67, "67"),
                arguments(82, "82")
        );
    }
}