package blog;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class Article {
    private final String name;
    private final String content;
    private final List<Comment> comments;

    public Article(String name, String content) {
        this(name, content, Collections.emptyList());
    }

    private Article(String name, String content, List<Comment> comments) {
        this.name = name;
        this.content = content;
        this.comments = comments;
    }

    private Comment addComment(
            String text,
            String author,
            LocalDate creationDate) throws CommentAlreadyExistException {
        var comment = new Comment(text, author, creationDate);

        if (comments.contains(comment)) {
            throw new CommentAlreadyExistException();
        }

        return comment;
    }

    public Article addComment(String text, String author) throws CommentAlreadyExistException {
        return new Article(
                name,
                content,
                Stream.concat(comments.stream(), Stream.of(addComment(text, author, LocalDate.now()))).toList()
        );
    }

    public List<Comment> getComments() {
        return comments;
    }
}

