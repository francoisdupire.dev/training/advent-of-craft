package blog;

import org.assertj.core.api.ThrowingConsumer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

import static blog.ArticleBuilder.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.instancio.Instancio.create;

class ArticleTests {
    private Article article;

    @Test
    void should_add_comment_in_an_article() throws CommentAlreadyExistException {
        Article commentedArticle = anArticle().build().addComment(COMMENT_TEXT, AUTHOR);

        assertThat(commentedArticle.getComments()).hasSize(1);
        assertComment(commentedArticle.getComments().get(0), COMMENT_TEXT, AUTHOR);
    }

    @Test
    void should_add_comment_in_an_article_containing_already_a_comment() throws Throwable {
        final var newComment = create(String.class);
        final var newAuthor = create(String.class);

        Article commentedArticle = anArticle().commented().build();

        Article twiceCommentedArticle = commentedArticle.addComment(newComment, newAuthor);

        assertThat(twiceCommentedArticle.getComments()).hasSize(2);
        assertComment(twiceCommentedArticle.getComments().getLast(), newComment, newAuthor);
    }

    @Nested
    class Fail {
        @Test
        void when__adding_an_existing_comment() throws CommentAlreadyExistException {
            var commentedArticle = anArticle().commented().build();

            assertThatThrownBy(() -> {
                commentedArticle.addComment(commentedArticle.getComments().get(0).text(), commentedArticle.getComments().get(0).author());
            }).isInstanceOf(CommentAlreadyExistException.class);
        }
    }

    @Nested
    class Immutability {
        @Test
        void should_return_new_article_after_adding_a_comment() throws CommentAlreadyExistException {
            var article = anArticle().build();

            Article commentedArticle = article.addComment(COMMENT_TEXT, AUTHOR);

            assertThat(commentedArticle).isNotSameAs(article);
        }
        @Test
        void should_not_alter_existing_article_when_adding_a_comment() throws CommentAlreadyExistException {
            var article = anArticle().build();

            article.addComment(COMMENT_TEXT, AUTHOR);

            assertThat(article.getComments()).isEmpty();
        }
        @Test
        void should_return_immutable_comments_list_for_uncommented_article() throws CommentAlreadyExistException {
            var article = anArticle().build();

            List<Comment> comments = article.getComments();

            assertThatThrownBy(() -> comments.add(new Comment("foo", "bar", LocalDate.now())))
                    .isInstanceOf(UnsupportedOperationException.class);
        }
        @Test
        void should_return_immutable_comments_list_for_commented_article() throws CommentAlreadyExistException {
            var article = anArticle().build();

            var commentedArticle = article.addComment("foo", "bar");

            List<Comment> comments = commentedArticle.getComments();

            assertThatThrownBy(() -> comments.add(new Comment("foo", "bar", LocalDate.now())))
                    .isInstanceOf(UnsupportedOperationException.class);
        }
    }

    private static void assertComment(Comment comment, String commentText, String author) {
        assertThat(comment.text()).isEqualTo(commentText);
        assertThat(comment.author()).isEqualTo(author);
    }
}