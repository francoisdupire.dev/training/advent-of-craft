package games;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

import static io.vavr.API.None;
import static io.vavr.API.Some;
import static org.assertj.core.api.Assertions.assertThat;

class FizzBuzzTests {
    private static final Random RANDOM = new Random();

    private static Stream<Arguments> validInputs() {
        return Stream.of(
                Arguments.of(1, "1"),
                Arguments.of(67, "67"),
                Arguments.of(82, "82"),
                Arguments.of(3, "Fizz"),
                Arguments.of(66, "Fizz"),
                Arguments.of(99, "Fizz"),
                Arguments.of(5, "Buzz"),
                Arguments.of(50, "Buzz"),
                Arguments.of(85, "Buzz"),
                Arguments.of(15, "FizzBuzz"),
                Arguments.of(30, "FizzBuzz"),
                Arguments.of(45, "FizzBuzz")
        );
    }

    private static Stream<Arguments> invalidInputs() {
        return Stream.of(
                Arguments.of(0),
                Arguments.of(-1),
                Arguments.of(101)
        );
    }

    @ParameterizedTest
    @MethodSource("validInputs")
    void parse_successfully_numbers_between_1_and_100(int input, String expectedResult) {
        assertThat(FizzBuzz.convert(input))
                .isEqualTo(Some(expectedResult));
    }

    @ParameterizedTest
    @MethodSource("invalidInputs")
    void parse_fail_for_numbers_out_of_range(int input) {
        assertThat(FizzBuzz.convert(input).isEmpty())
                .isTrue();
    }

    @RepeatedTest(100)
    void return_fizz_for_multiples_of_only_three() {
        int input = getMultipleOfOnlyThree();

        assertThat(FizzBuzz.convert(input))
                .as("%d should convert to Fizz", input)
                .isEqualTo(Some("Fizz"));
    }

    @RepeatedTest(100)
    void return_buzz_for_multiples_of_only_five() {
        int input = getMultipleOfOnlyFive();

        assertThat(FizzBuzz.convert(input))
                .as("%d should convert to Buzz", input)
                .isEqualTo(Some("Buzz"));
    }

    @RepeatedTest(100)
    void return_fizzbuzz_for_multiples_of_three_and_five() {
        int input = getMultipleOfThreeAndFive();

        assertThat(FizzBuzz.convert(input))
                .as("%d should convert to FizzBuzz", input)
                .isEqualTo(Some("FizzBuzz"));
    }

    @RepeatedTest(100)
    void return_number_for_other_in_range_numbers() {
        int input = getNotMultipleOfThreeNorFive();

        assertThat(FizzBuzz.convert(input))
                .as("%d should convert to itself", input)
                .isEqualTo(Some(String.valueOf(input)));
    }

    @RepeatedTest(100)
    void return_none_for_out_of_range_numbers() {
        int input = getOutOfRange();

        assertThat(FizzBuzz.convert(input))
                .as("%d should be out of range", input)
                .isEqualTo(None());
    }

    private static int getMultipleOfOnlyThree() {
        int result;
        do {
            result = RANDOM.nextInt(100) + 1;
        } while (notMultipleOf(result, 3) || multipleOf(result, 5));
        return result;
    }

    private static int getMultipleOfOnlyFive() {
        int result;
        do {
            result = RANDOM.nextInt(100) + 1;
        } while (notMultipleOf(result, 5) || multipleOf(result, 3));
        return result;
    }

    private static int getMultipleOfThreeAndFive() {
        int result;
        do {
            result = RANDOM.nextInt(100) + 1;
        } while (notMultipleOf(result, 3) || notMultipleOf(result, 5));
        return result;
    }

    private static int getNotMultipleOfThreeNorFive() {
        int result;
        do {
            result = RANDOM.nextInt(100) + 1;
        } while (multipleOf(result, 3) || multipleOf(result, 5));
        return result;
    }

    private static int getOutOfRange() {
        int result;
        do {
            result = RANDOM.nextInt(1000) - 500;
        } while (result >= 1 && result <= 100);
        return result;
    }

    private static boolean notMultipleOf(int number, int multiple) {
        return number % multiple != 0;
    }

    private static boolean multipleOf(int number, int multiple) {
        return number % multiple == 0;
    }
}