package account;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import static java.util.stream.Collectors.*;

public class Client {
    private final Map<String, BigDecimal> orderLines;

    public Client(Map<String, BigDecimal> orderLines) {
        this.orderLines = orderLines;
    }

    public String toStatement() {
        return orderLines.entrySet()
                         .stream()
                         .map(entry -> formatLine(entry.getKey(), entry.getValue()))
                         .collect(joining(System.lineSeparator(), "", System.lineSeparator() + "Total : " + getTotalAmount() + "€"));
    }

    private String formatLine(String name, BigDecimal value) {
        return name + " for " + value.setScale(2, RoundingMode.UNNECESSARY) + "€";
    }

    public BigDecimal getTotalAmount() {
        return orderLines.values()
                         .stream()
                         .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}

