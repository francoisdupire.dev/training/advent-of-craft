import account.Client;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.LinkedHashMap;

import static java.lang.System.lineSeparator;
import static org.assertj.core.api.Assertions.assertThat;

class ClientShould {
    private final Client client = new Client(new LinkedHashMap<>() {{
        put("Tenet Deluxe Edition", new BigDecimal("45.99"));
        put("Inception", new BigDecimal("30.50"));
        put("The Dark Knight", new BigDecimal("30.50"));
        put("Interstellar", new BigDecimal("23.98"));
    }});

    @Test
    void returnCorrectTotalAmount() {
        assertThat(client.getTotalAmount()).isEqualTo(new BigDecimal("130.97"));
    }

    @Test
    void returnCorrectStatement() {
        String statement = client.toStatement();

        assertThat(statement).isEqualTo(
                "Tenet Deluxe Edition for 45.99€" + lineSeparator() +
                        "Inception for 30.50€" + lineSeparator() +
                        "The Dark Knight for 30.50€" + lineSeparator() +
                        "Interstellar for 23.98€" + lineSeparator() +
                        "Total : 130.97€");
    }

    @Test
    void returnStatementTwice() {
        String firstStatement = client.toStatement();
        String secondStatement = client.toStatement();

        assertThat(firstStatement).isEqualTo(secondStatement);
    }

    @Test
    void roundTotalAmountToTwoDecimalPlaces() {
        Client client2 = new Client(new LinkedHashMap<>() {{
            put("Tenet Deluxe Edition", new BigDecimal("2.8"));
            put("Inception", new BigDecimal("0.17"));
        }});
        String statement = client2.toStatement();

        assertThat(statement).isEqualTo(
                "Tenet Deluxe Edition for 2.80€" + lineSeparator() +
                        "Inception for 0.17€" + lineSeparator() +
                        "Total : 2.97€");
    }
}