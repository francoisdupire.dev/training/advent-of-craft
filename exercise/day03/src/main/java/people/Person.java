package people;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Comparator.comparingInt;

public record Person(String firstName, String lastName, List<Pet> pets) {
    public Person(String firstName, String lastName) {
        this(firstName, lastName, new ArrayList<>());
    }

    public Person addPet(PetType petType, String name, int age) {
        pets.add(new Pet(petType, name, age));
        return this;
    }

    public boolean hasPets() {
        return !pets.isEmpty();
    }

    public Optional<Integer> youngestPetAge() {
        return youngestPet().map(Pet::age);
    }

    private Optional<Pet> youngestPet() {
        return pets.stream()
                   .min(comparingInt(Pet::age));
    }
}
