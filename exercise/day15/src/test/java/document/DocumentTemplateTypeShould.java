package document;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class DocumentTemplateTypeShould {
    @Test
    void failWhenNoMatchFoundForDocumentType() {
        assertThatThrownBy(() -> DocumentTemplateType.fromDocumentTypeAndRecordType("NOT_A_DOCUMENT_TYPE", "ALL"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Invalid Document template type or record type");
    }

    @Test
    void failWhenNoMatchFoundForRecordTypeAndDocumentTemplateRecordTypeNotAll() {
        assertThatThrownBy(() -> DocumentTemplateType.fromDocumentTypeAndRecordType("DEERPP", "NOT_A_RECORD_TYPE"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Invalid Document template type or record type");
    }

    @ParameterizedTest
    @MethodSource("documentTemplateAndRecordTypes")
    void returnDocumentTemplateTypeForDocumentTypeAndRecordType(String documentTemplateType, String recordType, DocumentTemplateType result) {
        assertThat(DocumentTemplateType.fromDocumentTypeAndRecordType(documentTemplateType, recordType))
                .isEqualTo(result);
    }

    @ParameterizedTest
    @EnumSource(RecordType.class)
    void returnDocumentTemplateTypeForSpecAndAnyRecordType(RecordType recordType) {
        assertThat(DocumentTemplateType.fromDocumentTypeAndRecordType("SPEC", recordType.name()))
                .isEqualTo(DocumentTemplateType.SPEC);
    }

    public static Stream<Arguments> documentTemplateAndRecordTypes() {
        return Stream.of(
                arguments("DEER", "INDIVIDUAL_PROSPECT", DocumentTemplateType.DEERPP),
                arguments("DEER", "LEGAL_PROSPECT", DocumentTemplateType.DEERPM),
                arguments("AUTP", "INDIVIDUAL_PROSPECT", DocumentTemplateType.AUTP),
                arguments("AUTM", "LEGAL_PROSPECT", DocumentTemplateType.AUTM),
                arguments("SPEC", "ALL", DocumentTemplateType.SPEC),
                arguments("GLPP", "INDIVIDUAL_PROSPECT", DocumentTemplateType.GLPP),
                arguments("GLPM", "LEGAL_PROSPECT", DocumentTemplateType.GLPM)
        );
    }
}