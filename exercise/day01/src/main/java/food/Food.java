package food;

import java.time.LocalDate;
import java.util.UUID;

public record Food(LocalDate expirationDate,
                   Boolean approvedForConsumption,
                   UUID inspectorId) {
    public boolean edibleOn(LocalDate date) {
        return stillFreshOn(date)
                && this.approvedForConsumption
                && hasBeenInspected();
    }

    private boolean stillFreshOn(LocalDate date) {
        return date.isEqual(this.expirationDate) || date.isBefore(this.expirationDate);
    }

    private boolean hasBeenInspected() {
        return this.inspectorId != null;
    }
}